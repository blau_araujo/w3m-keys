# Atalhos básicos do navegador w3m


![](pics/w3m-1.png)

> **Importante:** observe a caixa de texto das teclas alfabéticas!

## Atalhos essenciais

| Atalho | Descrição |
|---|---|
| `q` | Sair (pede confirmação). |
| `Q` | Sair sem confirmar. |
| `Ctrl+q` | Fechar aba. |
| `Ctrl+_` | Vai para a página inicial (definida em `WWW_HOME`). |
| `R` | Recarrega a página. |
| `U` | Abre url em novo buffer. |
| `B` | Fecha o buffer corrente e volta para a página anterior. |
| `s` | Abre o menu de buffers abertos. |
| `Ctrl+j` | Abre link em novo buffer. |
| `Ctrl+t` | Abre link em nova aba. |
| `M` | Abre página em um navegador externo. |
| `ESC-M` | Abre link em um navegador externo. |
| `T` | Abre nova aba com o buffer corrente. |
| `=` | Exibe informações sobre o buffer corrente. |
| `c` | Exibe a url corrente. |
| `u` | Exibe a url de destino. |
| `I` | Exibe a imagem em um visualizador. |
| `i` | Exibe a url da imagem. |
| `:` | Transforma strings de urls em urls. |
| `L` | Lista todas as urls da página em um buffer. |
| `ESC-m` | Abre menu de links e posiciona o cursor na seleção. |
| `ESC-l` | Abre menu de links e abre a seleção em um novo buffer. |
| `setas` | Move o cursor na direção das setas. |
| `h`, `j`, `k` e `l` | Teclas de direção do vi/vim. |
| `TAB` e `Shift+TAB` | Navega pelos links. |
| `[` e `]` | Move o cursor para o primeiro (`[`) ou o último (`]`) link. |
| `HOME` ou `g` | Vai para o início da página. |
| `END` ou `G` | Vai para o fim da página. |
| `Ctrl+g` | Exibe a posição do cursor e a codificação. |
| `ESC-g` | Vai para a linha especificada. |
| `SPC` ou `PgDn` | Rola uma página para baixo. |
| `-` ou `PgUp` | Rola uma página para cima. |
| `K` e `J` | Rolam a página uma linha para cima (`J`) ou para baixo (`K`). |
| `<` e `>` | Deslocam a tela para a direita (`>`) ou para a esquerda (`<`). |
| `,` e `.` | Deslocam a tela um caractere para a direita (`.`) ou para a esquerda (`,`). |
| `Ctrl+a` ou `^` | Move o cursor para o início da linha. |
| `Ctrl+e` ou `$` | Move o cursor para o fim da linha. |
| `z` | Centraliza a linha do cursor horizontalmente. |
| `Z` | Centraliza a linha do cursor verticalmente. |
| `{` e `}` | Vai para a aba da direita (`}`) ou da esquerda (`{`). |
| `ESC-a` | Adiciona página nos favoritos. |
| `ESC-b` | Abre a lista de favoritos. |
| `H` | Lista completa de atalhos. |
| `o` | Configurações. |





